package com.xenoterracide.jrs.fs.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties( "s3.credentials" )
public class S3CredentialsProperties {
    private String accessKeyId;
    private String secretAccessKey;

    public String getAccessKeyId() {
        return accessKeyId;
    }

    public void setAccessKeyId( final String accessKeyId ) {
        this.accessKeyId = accessKeyId;
    }

    public String getSecretAccessKey() {
        return secretAccessKey;
    }

    public void setSecretAccessKey( final String secretAccessKey ) {
        this.secretAccessKey = secretAccessKey;
    }
}
