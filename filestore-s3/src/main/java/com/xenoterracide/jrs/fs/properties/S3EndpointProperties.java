package com.xenoterracide.jrs.fs.properties;

import java.net.URI;
import java.util.Objects;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties( "s3.endpoint" )
public class S3EndpointProperties {
    private String region;
    private URI uri;

    public String getRegion() {
        return region;
    }

    public void setRegion( final String region ) {
        this.region = region;
    }

    public URI getUri() {
        return Objects.requireNonNull( uri );
    }

    public void setUri( final URI uri ) {
        this.uri = uri;
    }
}
