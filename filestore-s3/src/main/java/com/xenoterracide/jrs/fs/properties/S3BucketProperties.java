package com.xenoterracide.jrs.fs.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties( "s3.bucket" )
public class S3BucketProperties {
    private String name;

    public String getName() {
        return name;
    }

    public void setName( final String name ) {
        this.name = name;
    }
}
