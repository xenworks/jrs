package com.xenoterracide.jrs.fs;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Objects;

import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.Upload;
import com.amazonaws.services.s3.transfer.model.UploadResult;
import com.amazonaws.util.IOUtils;
import com.xenoterracide.logging.Loggable;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;
import org.apache.tika.mime.MimeTypes;

import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

/**
 * S3 implementation of the {@link FileStore}
 */
@Component
public class S3FileStore implements Loggable, FileStore {

    private final MimeTypes mimeTypes = MimeTypes.getDefaultMimeTypes();
    private final TransferManager transferManager;
    private final Bucket bucket;

    S3FileStore( final TransferManager transferManager, final Bucket bucket ) {
        this.transferManager = Objects.requireNonNull( transferManager );
        this.bucket = Objects.requireNonNull( bucket );
    }

    @Override
    public Resource get( final String path ) throws FileNotFoundException {
        log().debug( "Path: {}", path::toString );
        try {
            S3Object object = transferManager.getAmazonS3Client().getObject( bucket.getName(), path );
            return new S3InputStreamResource( object );
        }
        catch ( final AmazonS3Exception e ) {
            log().error( e );
            throw new FileNotFoundException( e.getMessage() );
        }
    }

    @Override
    public void put( final String filepath, final InputStream is ) throws IOException {
        Objects.requireNonNull( filepath );
        Objects.requireNonNull( is );
        byte[] bytes = IOUtils.toByteArray( is );
        if ( bytes.length == 0 ) {
            throw new IOException( "file empty" );
        }
        MediaType mt = mimeTypes.detect( new ByteArrayInputStream( bytes ), new Metadata() );
        ObjectMetadata meta = new ObjectMetadata();
        meta.setContentType( mt.toString() );
        meta.setContentLength( bytes.length );

        Upload upload = transferManager.upload( bucket.getName(), filepath, new ByteArrayInputStream( bytes ), meta );
        try {
            UploadResult result = upload.waitForUploadResult();
            log().info( "uploaded: {}", result::getKey );
        }
        catch ( InterruptedException e ) {
            log().error( e );
            throw new IOException( e );
        }
    }

    static class S3InputStreamResource extends InputStreamResource {

        private final Date lastModified;
        private final long length;


        S3InputStreamResource( final S3Object object ) {
            super( object.getObjectContent(), object.getKey() );
            this.length = object.getObjectMetadata().getContentLength();
            this.lastModified = object.getObjectMetadata().getLastModified();
        }

        @Override
        public long contentLength() throws IOException {
            return length;
        }

        @Override
        public long lastModified() throws IOException {
            return lastModified.getTime();
        }
    }
}
