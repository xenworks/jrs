package com.xenoterracide.jrs.fs;

import java.net.URI;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.AnonymousAWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.xenoterracide.logging.Loggable;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;


@Configuration
class S3Config implements Loggable {

    @Bean
    EndpointConfiguration endpointConfiguration(
        @Value( "${s3.endpoint.region:us-west-1}" ) final String region,
        @Value( "${s3.endpoint.uri}" ) final URI uri
    ) {
        log().info( "s3 {} {}", region::toString, uri::toString );
        return new EndpointConfiguration( uri.toString(), region );
    }

    @Bean
    AmazonS3 s3Client( final AWSStaticCredentialsProvider provider, final EndpointConfiguration configuration ) {
        return AmazonS3ClientBuilder
            .standard()
            .withPathStyleAccessEnabled( true )
            .withEndpointConfiguration( configuration )
            .withCredentials( provider )
            .build();
    }


    @Bean
    @ConditionalOnMissingBean
    AWSStaticCredentialsProvider credentialsProvider( final AWSCredentials credentials ) {
        log().info( "using {}", () -> credentials.getClass().getSimpleName() );
        return new AWSStaticCredentialsProvider( credentials );
    }

    @Bean
    @ConditionalOnMissingBean
    AWSCredentials anonCreds() {
        log().debug( "anonymous credentials" );
        return new AnonymousAWSCredentials();
    }

    @Bean
    @Primary
    @ConditionalOnProperty( prefix = "s3.credentials", value = {"accessKeyId", "secretAccessKey"} )
    AWSCredentials awsCredentials(
        @Value( "${s3.credentials.accessKeyId}" ) final String accessKeyId,
        @Value( "${s3.credentials.secretAccessKey}" ) final String secretKey
    ) {
        log().trace( "basic credentials: '{}' '{}'", accessKeyId::toString, secretKey::toString );
        return new BasicAWSCredentials( accessKeyId, secretKey );
    }

    @Bean
    @ConditionalOnMissingBean
    Bucket bucket( @Value( "${s3.bucket.name}" ) final String name ) {
        return new Bucket( name );
    }

    @Bean
    TransferManager transferManager( AmazonS3 client ) {
        return TransferManagerBuilder.standard().withS3Client( client ).build();
    }
}
