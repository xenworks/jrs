package com.xenoterracide.jrs.fs;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.Bucket;
import com.xenoterracide.logging.Loggable;
import io.findify.s3mock.S3Mock;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

@Order( 0 )
@Configuration
public class MockS3ServerConfig implements Loggable {

    @Bean( initMethod = "start", destroyMethod = "stop" )
    S3Mock s3() {
        return new S3Mock.Builder()
            .withInMemoryBackend()
            .build();
    }

    @Bean
    Bucket makeButcket( final AmazonS3 s3, final S3Mock mock ) {
        return s3.createBucket( "repo" );
    }
}
