package com.xenoterracide.jrs;


import com.xenoterracide.jrs.dm.config.RepositoriesProperties;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties( RepositoriesProperties.class )
public class Application {

    public static void main( final String... args ) throws Exception {
        SpringApplication.run( Application.class, args );
    }
}
