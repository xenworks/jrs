package com.xenoterracide.jrs;

import com.xenoterracide.jrs.test.AbstractIntegrationTest;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles( "e2e" )
@RunWith( SpringRunner.class )
@DirtiesContext( classMode = DirtiesContext.ClassMode.AFTER_CLASS )
@SpringBootTest( webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT )
public class MavenRepositoryCtrlE2EUnauthorizedTest extends AbstractIntegrationTest {

    @Autowired
    private TestRestTemplate rest;

    @Test
    public void createAndFind() throws Exception {
        ResponseEntity<String> res = rest.getForEntity( ARTIFACT + META, String.class );
        assertThat( res.getStatusCode() ).isEqualTo( HttpStatus.UNAUTHORIZED );


        /*
        mvc.perform( get( ARTIFACT + META ) ).andExpect( status().isNotFound() );

        mvc.perform( put( ARTIFACT + PATH + JAR + SHA1 ).content( jarSha1 ) ).andExpect( status().isCreated() );
        mvc.perform( put( ARTIFACT + PATH + JAR + MD5 ).content( jarMd5 ) ).andExpect( status().isCreated() );
        mvc.perform( put( ARTIFACT + PATH + JAR ).content( jar ) ).andExpect( status().isCreated() );

        mvc.perform( put( ARTIFACT + PATH + POM ).content( pom ) ).andExpect( status().isCreated() );
        mvc.perform( put( ARTIFACT + PATH + POM + SHA1 ).content( pomSha1 ) ).andExpect( status().isCreated() );
        mvc.perform( put( ARTIFACT + PATH + POM + MD5 ).content( pomMd5 ) ).andExpect( status().isCreated() );

        mvc.perform( put( ARTIFACT + META ).content( xml ) ).andExpect( status().isCreated() );

        mvc.perform( get( ARTIFACT + META ) ).andExpect( status().isOk() )
            .andExpect( content().string( xml ) )
            .andExpect( content().xml( xml ) );
            */
    }
}
