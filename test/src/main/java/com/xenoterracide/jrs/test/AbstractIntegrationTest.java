package com.xenoterracide.jrs.test;


import java.io.IOException;
import java.nio.charset.StandardCharsets;

import com.xenoterracide.util.IntegrationTest;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.experimental.categories.Category;

import org.springframework.core.io.ClassPathResource;

@Category( IntegrationTest.class )
public abstract class AbstractIntegrationTest {


    public static final String ROOT = "/repository/snapshots/";
    public static final String ARTIFACT = ROOT + "com/xenoterracide/fake/1.0-SNAPSHOT/";
    public static final String META = "maven-metadata.xml";
    public static final String PATH = "fake-1.0-20170602.135448-1.";
    public static final String JAR = "jar";
    public static final String POM = "pom";
    public static final String SHA1 = ".sha1";
    public static final String MD5 = ".md5";

    private String xml;
    private String jarSha1;
    private String jarMd5;
    private String pom;
    private String pomSha1;
    private String pomMd5;
    private byte[] jar;

    @Before
    public void setup() throws IOException {
        String jarFile = "fake-1.0-SNAPSHOT.jar";
        String pomFile = "fake.pom";

        this.jar = IOUtils.toByteArray( new ClassPathResource( jarFile ).getInputStream() );
        this.xml = IOUtils.toString( new ClassPathResource( META ).getInputStream(), StandardCharsets.UTF_8 );
        this.jarMd5 = IOUtils.toString( new ClassPathResource( JAR + MD5 ).getInputStream(), StandardCharsets.UTF_8 );
        this.jarSha1 = IOUtils.toString( new ClassPathResource( JAR + SHA1 ).getInputStream(), StandardCharsets.UTF_8 );
        this.pom = IOUtils.toString( new ClassPathResource( pomFile ).getInputStream(), StandardCharsets.UTF_8 );
        this.pomMd5 = IOUtils.toString( new ClassPathResource( POM + MD5 ).getInputStream(), StandardCharsets.UTF_8 );
        this.pomSha1 = IOUtils.toString( new ClassPathResource( POM + SHA1 ).getInputStream(), StandardCharsets.UTF_8 );
    }

    protected String getXml() {
        return xml;
    }

    protected String getJarSha1() {
        return jarSha1;
    }

    protected String getJarMd5() {
        return jarMd5;
    }

    protected String getPom() {
        return pom;
    }

    protected String getPomSha1() {
        return pomSha1;
    }

    protected String getPomMd5() {
        return pomMd5;
    }

    protected byte[] getJar() {
        return jar;
    }
}
