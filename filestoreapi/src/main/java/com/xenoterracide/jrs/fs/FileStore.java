package com.xenoterracide.jrs.fs;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.core.io.Resource;

public interface FileStore {
    Resource get( String path ) throws FileNotFoundException;

    void put( String filepath, InputStream is ) throws IOException;
}
