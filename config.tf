variable "region" {
    type = "string"
    default = "us-east-1"
}

terraform {
    backend "s3" {
        bucket = "com_xenoterracide_terraform_prod"
        key = "reliquary-test.tfstate"
        region = "${var.region}"
    }
}

data "aws_region" "current" {
    name = "${var.region}"
}

provider "aws" {
    region = "${var.region}"
}

data "terraform_remote_state" "tfs" {
    backend = "s3"
    config {
        bucket = "com_xenoterracide_terraform_prod"
        key = "reliquary-test.tfstate"
        region = "${data.aws_region.current.name}"
    }
}

data "aws_availability_zones" "available" {
}

data "aws_availability_zone" "Selected" {
    name = "${data.aws_availability_zones.available.names[1]}"
}

resource "aws_s3_bucket" "TestBucket" {
    acl = "private"
    bucket = "${uuid()}.jsr.xenoterracide.com"
    versioning {
        enabled = true
    }
}
