package com.xenoterracide.jrs.dm;

import java.util.Optional;

@FunctionalInterface
public interface Repositories {

    Optional<RepositorySettings> getSettings( String repository );
}
