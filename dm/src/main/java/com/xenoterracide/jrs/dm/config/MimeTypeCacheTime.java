package com.xenoterracide.jrs.dm.config;


import java.util.concurrent.TimeUnit;

public class MimeTypeCacheTime {
    private long time;
    private TimeUnit unit;

    public MimeTypeCacheTime( final long time, final TimeUnit unit ) {
        this.time = time;
        this.unit = unit;
    }

    public MimeTypeCacheTime() {
    }

    public long getTime() {
        return time;
    }

    public void setTime( final long time ) {
        this.time = time;
    }

    public TimeUnit getUnit() {
        return unit;
    }

    public void setUnit( final TimeUnit unit ) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        return "MimeTypeCacheTime{" +
            "time=" + time +
            ", unit=" + unit +
            '}';
    }
}
