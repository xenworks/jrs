package com.xenoterracide.jrs.dm;

import java.util.Objects;

import org.apache.tika.mime.MediaType;
import org.apache.tika.mime.MimeType;

public final class MimeTypes {

    public static boolean supported( final MimeType type ) {
        return MediaType.set(
            MediaType.APPLICATION_XML,
            MediaType.TEXT_PLAIN,
            MediaType.application( "java-archive" )
        ).contains( Objects.requireNonNull( type ).getType() );
    }
}
