package com.xenoterracide.jrs.dm.config;

public enum RepositoryType {
    RELEASE,
    SNAPSHOT;
}
