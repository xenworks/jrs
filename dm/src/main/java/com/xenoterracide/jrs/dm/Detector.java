package com.xenoterracide.jrs.dm;

import java.io.InputStream;

import org.apache.tika.mime.MimeType;
import org.apache.tika.mime.MimeTypeException;

public interface Detector {

    MimeType detect( InputStream stream, String filename ) throws IllegalArgumentException;

    MimeType getMimeType( String mimeType ) throws MimeTypeException;
}
