package com.xenoterracide.jrs.dm.config;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import com.xenoterracide.jrs.dm.RepositorySettings;
import org.apache.commons.io.FilenameUtils;

public class RepositorySettingsImpl implements RepositorySettings {
    private RepositoryType type;
    private Map<String, MimeTypeCacheTime> mimeTypeCache = new HashMap<>();

    public RepositorySettingsImpl() {
    }

    @Override
    public RepositoryType getType() {
        return type;
    }

    public void setType( final RepositoryType type ) {
        this.type = type;
    }

    @Override
    public Optional<MimeTypeCacheTime> forFilename( final String file ) {
        String extension = FilenameUtils.getExtension( Objects.requireNonNull( file ) );
        return Optional.ofNullable( mimeTypeCache.get( extension ) );
    }

    public Map<String, MimeTypeCacheTime> getMimeTypeCache() {
        return mimeTypeCache;
    }

    public void setMimeTypeCache( final Map<String, MimeTypeCacheTime> mimeTypeCache ) {
        this.mimeTypeCache = mimeTypeCache;
    }

    @Override
    public String toString() {
        return "RepositorySettingsImpl{" +
            "type=" + type +
            ", mimeTypeCache=" + mimeTypeCache +
            '}';
    }
}
