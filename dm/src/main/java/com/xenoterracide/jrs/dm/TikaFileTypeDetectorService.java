package com.xenoterracide.jrs.dm;


import java.io.IOException;
import java.io.InputStream;

import com.xenoterracide.logging.Loggable;
import org.apache.tika.Tika;
import org.apache.tika.mime.MimeType;
import org.apache.tika.mime.MimeTypeException;
import org.apache.tika.mime.MimeTypes;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

@Service
class TikaFileTypeDetectorService implements Detector, Loggable {

    @Bean
    MimeTypes mimeTypes() {
        return MimeTypes.getDefaultMimeTypes();
    }

    @Bean
    Tika tika() {
        return new Tika();
    }

    @Override
    public MimeType detect( final InputStream stream, final String filename ) throws IllegalArgumentException {
        try {
            return getMimeType( tika().detect( stream, filename ) );
        }
        catch ( IOException e ) {
            log().error( e );
            throw new IllegalArgumentException( e );
        }
    }

    @Override
    public MimeType getMimeType( final String mimeType ) throws IllegalArgumentException {
        try {
            return mimeTypes().forName( mimeType );
        }
        catch ( MimeTypeException e ) {
            log().error( e );
            throw new IllegalArgumentException( e );
        }
    }
}
