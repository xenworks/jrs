package com.xenoterracide.jrs.dm;


import java.util.Optional;

import com.xenoterracide.jrs.dm.config.MimeTypeCacheTime;
import com.xenoterracide.jrs.dm.config.RepositoryType;

public interface RepositorySettings {
    RepositoryType getType();

    Optional<MimeTypeCacheTime> forFilename( String extension );
}
