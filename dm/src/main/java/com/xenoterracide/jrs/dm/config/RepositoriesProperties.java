package com.xenoterracide.jrs.dm.config;


import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Function;

import com.xenoterracide.jrs.dm.Repositories;
import com.xenoterracide.jrs.dm.RepositorySettings;
import com.xenoterracide.logging.Loggable;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties
public class RepositoriesProperties implements Repositories, Loggable {
    private Map<String, RepositorySettingsImpl> repositories = new HashMap<>();

    public RepositoriesProperties() {
        RepositorySettingsImpl snapshot = new RepositorySettingsImpl();
        snapshot.setType( RepositoryType.SNAPSHOT );
        Collection<String> validExtensions = Arrays.asList( "xml", "md5", "sha1", "jar" );
        validExtensions.forEach( cacheTime( snapshot, RepositoriesProperties::snapshot ) );

        RepositorySettingsImpl release = new RepositorySettingsImpl();
        release.setType( RepositoryType.RELEASE );
        validExtensions.forEach( cacheTime( snapshot, RepositoriesProperties::release ) );

        repositories.put( "snapshots", snapshot );
        repositories.put( "releases", release );
    }

    public Map<String, RepositorySettingsImpl> getRepositories() {
        return repositories;
    }

    public void setRepositories( final Map<String, RepositorySettingsImpl> repositories ) {
        this.repositories = repositories;
    }

    @Override
    public Optional<RepositorySettings> getSettings( final String repository ) {
        return Optional.ofNullable( repositories.get( Objects.requireNonNull( repository ) ) );
    }

    @Override
    public String toString() {
        return "RepositoriesProperties{" +
            "repositories=" + repositories +
            '}';
    }

    private static Consumer<String> cacheTime(
        RepositorySettingsImpl settings, Function<String, MimeTypeCacheTime> calcCache
    ) {
        return key -> settings.getMimeTypeCache().computeIfAbsent( key, calcCache );
    }

    private static MimeTypeCacheTime snapshot( final String key ) {
        return new MimeTypeCacheTime( 10, TimeUnit.SECONDS );
    }

    private static MimeTypeCacheTime release( final String key ) {
        return new MimeTypeCacheTime( 1, TimeUnit.DAYS );
    }
}
