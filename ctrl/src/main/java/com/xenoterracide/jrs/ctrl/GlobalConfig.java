package com.xenoterracide.jrs.ctrl;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource( "classpath:global.properties" )
class GlobalConfig {
}
