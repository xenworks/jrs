package com.xenoterracide.jrs.ctrl;


import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;

import com.xenoterracide.jrs.dm.Detector;
import com.xenoterracide.jrs.dm.MimeTypes;
import com.xenoterracide.jrs.dm.Repositories;
import com.xenoterracide.jrs.dm.RepositorySettings;
import com.xenoterracide.jrs.dm.config.MimeTypeCacheTime;
import com.xenoterracide.jrs.fs.FileStore;
import com.xenoterracide.logging.Loggable;
import org.apache.commons.io.IOUtils;
import org.apache.tika.mime.MimeType;
import org.apache.tika.mime.MimeTypeException;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.io.Resource;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.InvalidMimeTypeException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
@ComponentScan( "com.xenoterracide.files" )
@RequestMapping( "/maven2/{repository}/" )
public class MavenRepositoryCtrl implements Loggable {

    private final FileStore fileStore;
    private final Repositories repositories;
    private final Detector detector;

    MavenRepositoryCtrl( final FileStore fileStore,
                         final Repositories repositories,
                         final Detector detector ) {
        this.fileStore = Objects.requireNonNull( fileStore );
        this.repositories = Objects.requireNonNull( repositories );
        this.detector = Objects.requireNonNull( detector );
        log().info( "repositories {}", repositories::toString );
    }

    @ResponseBody
    @GetMapping( "**/{filename:[\\.0-9a-z-]+}" )
    ResponseEntity<Resource> getFile(
        @PathVariable final String repository,
        @PathVariable final String filename,
        final HttpServletRequest request )
        throws IOException, MimeTypeException {

        RepositorySettings settings = repositories.getSettings( repository ).orElseThrow( FileNotFoundException::new );
        MimeTypeCacheTime cacheTime = settings.forFilename( filename ).orElseThrow( FileNotFoundException::new );

        Resource resource = fileStore.get( requestToPath( request ) );

        return ResponseEntity.ok()
            .cacheControl( CacheControl.maxAge( cacheTime.getTime(), cacheTime.getUnit() ) )
            .body( resource );
    }

    @PutMapping( "**" )
    @ResponseStatus( HttpStatus.CREATED )
    void putFile( final HttpServletRequest request ) throws IOException {
        byte[] bytes = IOUtils.toByteArray( request.getInputStream() );
        MimeType detect = detector.detect( new ByteArrayInputStream( bytes ), request.getRequestURI() );
        if ( !MimeTypes.supported( detect ) ) {
            throw new InvalidMimeTypeException( detect.getType().getType(), "not supported" );
        }
        fileStore.put( requestToPath( request ), new ByteArrayInputStream( bytes ) );
    }

    private String requestToPath( HttpServletRequest request ) {
        String path = request.getRequestURI().substring( 1 );
        log().debug( "{} {}", request::getMethod, path::toString );
        return path;
    }
}
