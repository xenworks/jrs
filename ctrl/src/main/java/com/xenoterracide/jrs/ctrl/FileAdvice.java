package com.xenoterracide.jrs.ctrl;

import java.io.FileNotFoundException;
import java.nio.file.FileAlreadyExistsException;

import org.springframework.http.HttpStatus;
import org.springframework.util.InvalidMimeTypeException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
class FileAdvice {


    @ExceptionHandler( FileAlreadyExistsException.class )
    @ResponseStatus( value = HttpStatus.CONFLICT, reason = "This File has been previously uploaded" )
    void fileExists() {
    }

    @ResponseBody
    @ExceptionHandler( FileNotFoundException.class )
    @ResponseStatus( value = HttpStatus.NOT_FOUND, reason = "File not found" )
    void notFound() {
    }

    @ExceptionHandler( InvalidMimeTypeException.class )
    @ResponseStatus( value = HttpStatus.BAD_REQUEST, reason = "Invalid Mime Type" )
    void mimeType() {
    }
}
