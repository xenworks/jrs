package com.xenoterracide.jrs.ctrl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import com.xenoterracide.jrs.dm.Detector;
import com.xenoterracide.jrs.dm.Repositories;
import com.xenoterracide.jrs.dm.config.MimeTypeCacheTime;
import com.xenoterracide.jrs.dm.config.RepositorySettingsImpl;
import com.xenoterracide.jrs.dm.config.RepositoryType;
import com.xenoterracide.jrs.fs.FileStore;
import org.apache.commons.io.IOUtils;
import org.apache.tika.mime.MimeTypes;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith( SpringRunner.class )
@WebMvcTest( MavenRepositoryCtrl.class )
public class MavenRepositoryCtrlMavenMetadataXmlTest {

    private static final String ROOT = "maven2/snapshots/";
    private static final String PATH = ROOT + "com/xenoterracide/fake/1.0-SNAPSHOT/maven-metadata.xml";

    @Autowired
    private MockMvc mvc;

    @MockBean
    private FileStore fileStore;
    @MockBean
    private Repositories repositories;
    @MockBean
    private Detector detector;

    private Resource resource;
    private String xml;

    @Before
    public void setup() throws IOException {
        String filename = "maven-metadata.xml";
        this.resource = new ClassPathResource( filename );
        this.xml = IOUtils.toString( new ClassPathResource( filename ).getInputStream(), StandardCharsets.UTF_8 );
    }

    @Test
    public void createAndFind() throws Exception {
        when( fileStore.get( argThat( equalTo( PATH ) ) ) )
            .thenThrow( new FileNotFoundException() )
            .thenReturn( resource );

        RepositorySettingsImpl snapshot = new RepositorySettingsImpl();
        snapshot.setType( RepositoryType.SNAPSHOT );
        snapshot.getMimeTypeCache().put( "xml", new MimeTypeCacheTime( 10, TimeUnit.SECONDS ) );

        when( detector.detect( any(), anyString() ) )
            .thenReturn( MimeTypes.getDefaultMimeTypes().forName( MimeTypes.XML ) );

        when( repositories.getSettings( anyString() ) ).thenReturn( Optional.of( snapshot ) );

        mvc.perform( get( '/' + PATH ) ).andExpect( status().isNotFound() );


        mvc.perform( put( '/' + PATH ).content( xml ) ).andExpect( status().isCreated() );
        mvc.perform( get( '/' + PATH ) ).andExpect( status().isOk() )
            .andExpect( content().string( xml ) )
            .andExpect( content().xml( xml ) );


        verify( fileStore, times( 1 ) )
            .put( argThat( equalTo( PATH ) ), argThat( instanceOf( InputStream.class ) ) );
        verify( fileStore, times( 2 ) )
            .get( argThat( equalTo( PATH ) ) );


    }
}
