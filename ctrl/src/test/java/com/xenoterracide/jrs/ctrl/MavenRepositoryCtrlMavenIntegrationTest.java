package com.xenoterracide.jrs.ctrl;

import com.xenoterracide.jrs.TestApplication;
import com.xenoterracide.jrs.fs.MockS3ServerConfig;
import com.xenoterracide.jrs.test.AbstractIntegrationTest;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@RunWith( SpringRunner.class )
@SpringBootTest( classes = {TestApplication.class, MockS3ServerConfig.class} )
public class MavenRepositoryCtrlMavenIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void createAndFind() throws Exception {
        mvc.perform( get( ARTIFACT + META ) ).andExpect( status().isNotFound() );

        mvc.perform( put( ARTIFACT + PATH + JAR + SHA1 ).content( getJarSha1() ) ).andExpect( status().isCreated() );
        mvc.perform( put( ARTIFACT + PATH + JAR + MD5 ).content( getJarMd5() ) ).andExpect( status().isCreated() );
        mvc.perform( put( ARTIFACT + PATH + JAR ).content( getJar() ) ).andExpect( status().isCreated() );

        mvc.perform( put( ARTIFACT + PATH + POM ).content( getPom() ) ).andExpect( status().isCreated() );
        mvc.perform( put( ARTIFACT + PATH + POM + SHA1 ).content( getPomSha1() ) ).andExpect( status().isCreated() );
        mvc.perform( put( ARTIFACT + PATH + POM + MD5 ).content( getPomMd5() ) ).andExpect( status().isCreated() );

        mvc.perform( put( ARTIFACT + META ).content( getXml() ) ).andExpect( status().isCreated() );

        mvc.perform( get( ARTIFACT + META ) ).andExpect( status().isOk() )
            .andExpect( content().string( getXml() ) )
            .andExpect( content().xml( getXml() ) );
    }

}
